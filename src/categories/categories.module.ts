import { Module } from '@nestjs/common';
import { CategoriesResolver } from './categories.resolver';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import * as dotenv from 'dotenv';
import { protobufPackage } from './categories.pb';

dotenv.config()
@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'Categories',
        transport: Transport.GRPC,
        options: {
          url: process.env.MS_RECIPE_URL,
          package: protobufPackage,
          protoPath: join('node_modules/protos/proto/categories.proto'),
        },
      }
    ])
  ],
  providers: [CategoriesResolver,],
})
export class CategoriesModule {}
