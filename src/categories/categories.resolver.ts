import { Inject, OnModuleInit } from '@nestjs/common';
import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { ClientGrpc } from '@nestjs/microservices';
import { Observable, map } from 'rxjs';
import { CategoriesResponse, CategoriesServiceClient, Category, FindRequest, AllCategories} from './categories.pb';

@Resolver('Categories')
export class CategoriesResolver implements OnModuleInit{
  private categoriesService: CategoriesServiceClient;
  constructor(@Inject('Categories') private client: ClientGrpc) {}

  onModuleInit() {
    this.categoriesService = this.client.getService<CategoriesServiceClient>('CategoriesService');
  }

  @Mutation('loadCategory')
  async loadCategory(): Promise<Observable<CategoriesResponse>> {
    return this.categoriesService.loadCategory({})
  }

  @Query('categories')
  async findAll(): Promise<Observable<AllCategories>> {
  return this.categoriesService.findAll({})
  }

  @Query('category')
  async findOne(@Args('input') input: FindRequest): Promise<Observable<Category>> {
    return this.categoriesService.findOne(input)
  }
}
