/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";

export const protobufPackage = "categories";

export interface Empty {
}

export interface Error {
  message: string;
}

export interface Category {
  id: string;
  name: string;
}

export interface AllCategories {
  categories: Category[];
  error: Error | undefined;
}

export interface CategoriesResponse {
  success: boolean;
  error: Error | undefined;
}

export interface FindRequest {
  id: string;
}

export const CATEGORIES_PACKAGE_NAME = "categories";

export interface CategoriesServiceClient {
  loadCategory(request: Empty): Observable<CategoriesResponse>;

  findAll(request: Empty): Observable<AllCategories>;

  findOne(request: FindRequest): Observable<Category>;
}

export interface CategoriesServiceController {
  loadCategory(request: Empty): Promise<CategoriesResponse> | Observable<CategoriesResponse> | CategoriesResponse;

  findAll(request: Empty): Promise<AllCategories> | Observable<AllCategories> | AllCategories;

  findOne(request: FindRequest): Promise<Category> | Observable<Category> | Category;
}

export function CategoriesServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["loadCategory", "findAll", "findOne"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("CategoriesService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("CategoriesService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const CATEGORIES_SERVICE_NAME = "CategoriesService";
