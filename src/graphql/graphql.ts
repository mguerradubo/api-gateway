
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export class FindRequest {
    id: string;
}

export class CommentRequest {
    text: string;
    email: string;
    recipeId: string;
}

export class RemoveRequest {
    id: string;
    recipeId: string;
}

export class InputLimit {
    limit: number;
}

export class InputQuery {
    query: string;
}

export class InputLetter {
    letter: string;
}

export class InputCategory {
    category: string;
}

export class LoginRequest {
    email: string;
    password: string;
}

export class CreateUserRequest {
    name: string;
    lastname: string;
    email: string;
    password: string;
}

export class GetUserByEmailRequest {
    email: string;
}

export class Category {
    id: string;
    name: string;
}

export class Error {
    message: string;
}

export class CategoriesResponse {
    success?: Nullable<boolean>;
    error?: Nullable<Error>;
}

export class AllCategories {
    categories?: Nullable<Nullable<Category>[]>;
    error?: Nullable<Error>;
}

export abstract class IQuery {
    abstract categories(): Nullable<AllCategories> | Promise<Nullable<AllCategories>>;

    abstract category(input?: Nullable<FindRequest>): Nullable<Category> | Promise<Nullable<Category>>;

    abstract comments(): Nullable<AllComments> | Promise<Nullable<AllComments>>;

    abstract random_ingredient(input?: Nullable<InputLimit>): AllIngredients | Promise<AllIngredients>;

    abstract ingredients(): AllIngredients | Promise<AllIngredients>;

    abstract ingredient(input?: Nullable<FindRequest>): Nullable<Ingredient> | Promise<Nullable<Ingredient>>;

    abstract recipes(): AllRecipes | Promise<AllRecipes>;

    abstract recipe(input?: Nullable<FindRequest>): Recipe | Promise<Recipe>;

    abstract random_recipe(input?: Nullable<InputLimit>): AllRecipes | Promise<AllRecipes>;

    abstract category_recipes(input?: Nullable<InputCategory>): AllRecipes | Promise<AllRecipes>;

    abstract searchRecipe(input?: Nullable<InputQuery>): AllRecipes | Promise<AllRecipes>;

    abstract user(input?: Nullable<GetUserByEmailRequest>): Nullable<GetUserByEmailResponse> | Promise<Nullable<GetUserByEmailResponse>>;
}

export abstract class IMutation {
    abstract loadCategory(): Nullable<CategoriesResponse> | Promise<Nullable<CategoriesResponse>>;

    abstract addComment(input?: Nullable<CommentRequest>): Nullable<Comment> | Promise<Nullable<Comment>>;

    abstract removeComment(input?: Nullable<RemoveRequest>): Nullable<CommentsResponse> | Promise<Nullable<CommentsResponse>>;

    abstract loadIngredients(): IngredientResponse | Promise<IngredientResponse>;

    abstract loadRecipes(input?: Nullable<InputLetter>): RecipesResponse | Promise<RecipesResponse>;

    abstract signUp(input?: Nullable<CreateUserRequest>): Nullable<string> | Promise<Nullable<string>>;

    abstract login(input?: Nullable<LoginRequest>): Nullable<string> | Promise<Nullable<string>>;
}

export class Comment {
    id: string;
    text: string;
    email: string;
    createdAt: string;
    recipeId: string;
}

export class CommentsResponse {
    success: boolean;
    error?: Nullable<Error>;
}

export class AllComments {
    comments?: Nullable<Nullable<Comment>[]>;
    error?: Nullable<Error>;
}

export abstract class ISubscription {
    abstract commentAdded(recipeId: string): Nullable<Comment> | Promise<Nullable<Comment>>;
}

export class Ingredient {
    id: string;
    name: string;
    description: string;
    image: string;
}

export class IngredientResponse {
    success: boolean;
    errors?: Nullable<Nullable<Error>[]>;
}

export class AllIngredients {
    ingredients?: Nullable<Nullable<Ingredient>[]>;
    errors?: Nullable<Error>;
}

export class Recipe {
    id: string;
    name: string;
    category: string;
    instruction: string;
    ingredients?: Nullable<Nullable<Ingredient>[]>;
    measure?: Nullable<Nullable<string>[]>;
    image?: Nullable<string>;
    video?: Nullable<string>;
    comments?: Nullable<Nullable<Comment>[]>;
}

export class RecipesResponse {
    success: boolean;
    error?: Nullable<Error>;
}

export class AllRecipes {
    recipes?: Nullable<Nullable<Recipe>[]>;
    error?: Nullable<Error>;
}

export class User {
    id: string;
    name: string;
    lastname: string;
    email: string;
    password: string;
}

export class GetUserByEmailResponse {
    id: string;
    name: string;
    lastname: string;
    email: string;
}

type Nullable<T> = T | null;
