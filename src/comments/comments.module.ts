import { Module } from '@nestjs/common';
import { CommentsResolver } from './comments.resolver';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import * as dotenv from 'dotenv';
import { protobufPackage } from './comments.pb';

dotenv.config()
@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'Comments',
        transport: Transport.GRPC,
        options: {
          url: process.env.MS_RECIPE_URL,
          package: protobufPackage,
          protoPath: join('node_modules/protos/proto/comments.proto'),
        },
      }
    ])
  ],
  providers: [CommentsResolver,],
})
export class CommentsModule {}
