import { Inject, OnModuleInit } from '@nestjs/common';
import { Resolver, Query, Mutation, Args, Subscription} from '@nestjs/graphql';
import { ClientGrpc } from '@nestjs/microservices';
import { Observable, map } from 'rxjs';
import { CommentRequest, CommentsServiceClient, RemoveRequest, AllComments, CommentsResponse} from './comments.pb';
import { PubSub } from 'graphql-subscriptions';
import { Comment } from 'src/graphql/graphql';

@Resolver('Comments')
export class CommentsResolver implements OnModuleInit {
  private commentsService: CommentsServiceClient;
  constructor(@Inject('Comments') private client: ClientGrpc) {}

  onModuleInit() {
    this.commentsService = this.client.getService<CommentsServiceClient>('CommentsService');
  }
  private pubSub = new PubSub();
  @Query('comments')
  async findAll(): Promise<Observable<AllComments>>{
    return this.commentsService.findAll({})
  }

  
  @Mutation('addComment')
  async addComment(@Args('input') input: CommentRequest): Promise<Comment>{
    let id: string;
    let text: string;
    let recipeId: string;
    let email: string;
    let createdAt: string;
  
    const comment = await this.commentsService.createComment(input).toPromise();
  
    id = comment.id;
    text = comment.text;
    recipeId = comment.recipeId;
    email = comment.email;
    createdAt = comment.createdAt;
  
    const newComment = {id, text, recipeId, email, createdAt};
  
    this.pubSub.publish('commentAdded', { commentAdded: newComment, recipeId: input.recipeId });
  
    return newComment;
  }

  @Mutation('removeComment')
  async removeComment(@Args('input') input: RemoveRequest): Promise<Observable<CommentsResponse>>{
    return this.commentsService.deleteComment(input);
  }


  @Subscription(() => Comment, {
    filter: (payload, variables) => {
      return payload.recipeId === variables.recipeId;
    },
  })
  commentAdded(@Args('recipeId') recipeId: string) {
    return this.pubSub.asyncIterator('commentAdded');
  }
}
