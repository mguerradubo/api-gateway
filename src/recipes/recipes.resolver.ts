import { Inject, OnModuleInit } from '@nestjs/common';
import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { ClientGrpc } from '@nestjs/microservices';
import { Observable, map } from 'rxjs';
import { AllRecipes, FindRequest, InputLetter, RecipesResponse, RecipesServiceClient, Recipe, InputLimit, InputCategory, InputQuery} from './recipes.pb';

@Resolver('Recipes')
export class RecipesResolver implements OnModuleInit{
  private recipesService: RecipesServiceClient;
  constructor(@Inject('Recipes') private client: ClientGrpc) {}

  onModuleInit() {
    this.recipesService = this.client.getService<RecipesServiceClient>('RecipesService');
  }
  
  @Mutation('loadRecipes')
  async loadRecipes(@Args('input') input: InputLetter): Promise<Observable<RecipesResponse>> {
    return this.recipesService.loadRecipes(input)
  }

  @Query('recipes')
  async findAll(): Promise<Observable<AllRecipes>> {
    return this.recipesService.findAll({})
  }

  @Query('recipe')
  async findOne(@Args('input') input: FindRequest): Promise<Observable<Recipe>> {
    return this.recipesService.findOne(input)
  }

  @Query('random_recipe')
  async findRandom(@Args('input') input: InputLimit): Promise<Observable<AllRecipes>> {
    return this.recipesService.findRandom(input)
  }

  @Query('category_recipes')
  async findCategoryRecipes(@Args('input') input: InputCategory): Promise<Observable<AllRecipes>> {
    return this.recipesService.findCategoryRecipes(input)
  }

  @Query('searchRecipe')
  async searchRecipe(@Args('input') input: InputQuery): Promise<Observable<AllRecipes>> {
    return this.recipesService.searchRecipes(input)
  }
}
