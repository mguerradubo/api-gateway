/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";

export const protobufPackage = "comments";

export interface Empty {
}

export interface Error {
  message: string;
}

export interface Comment {
  id: string;
  text: string;
  email: string;
  createdAt: string;
  recipeId: string;
}

export interface AllComments {
  comments: Comment[];
  error: Error | undefined;
}

export interface CommentRequest {
  email: string;
  recipeId: string;
  text: string;
}

export interface CommentsResponse {
  success: boolean;
  error: Error | undefined;
}

export interface RemoveRequest {
  id: string;
  recipeId: string;
}

export const COMMENTS_PACKAGE_NAME = "comments";

export interface CommentsServiceClient {
  createComment(request: CommentRequest): Observable<Comment>;

  deleteComment(request: RemoveRequest): Observable<CommentsResponse>;

  findAll(request: Empty): Observable<AllComments>;
}

export interface CommentsServiceController {
  createComment(request: CommentRequest): Promise<Comment> | Observable<Comment> | Comment;

  deleteComment(request: RemoveRequest): Promise<CommentsResponse> | Observable<CommentsResponse> | CommentsResponse;

  findAll(request: Empty): Promise<AllComments> | Observable<AllComments> | AllComments;
}

export function CommentsServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["createComment", "deleteComment", "findAll"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("CommentsService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("CommentsService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const COMMENTS_SERVICE_NAME = "CommentsService";
