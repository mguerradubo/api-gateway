import { Module } from '@nestjs/common';
import { RecipesResolver } from './recipes.resolver';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import * as dotenv from 'dotenv';
import { protobufPackage } from './recipes.pb';

dotenv.config()
@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'Recipes',
        transport: Transport.GRPC,
        options: {
          url: process.env.MS_RECIPE_URL,
          package: protobufPackage,
          protoPath: join('node_modules/protos/proto/recipes.proto'),
        },
      }
    ])
  ],
  providers: [RecipesResolver,],
})
export class RecipesModule {}
