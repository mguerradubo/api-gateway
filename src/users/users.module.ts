import { Module } from '@nestjs/common';
import { UsersResolver } from './users.resolver';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { protobufPackage } from './users.pb';
import { join } from 'path';
import * as dotenv from 'dotenv';

dotenv.config()
@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'Users',
        transport: Transport.GRPC,
        options: {
          url: process.env.MS_USER_URL,
          package: protobufPackage,
          protoPath: join('node_modules/protos/proto/users.proto'),
        },
      }
    ]),
  ],
  providers: [UsersResolver, ],
})
export class UsersModule {}
