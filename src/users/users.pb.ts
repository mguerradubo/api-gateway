/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";

export const protobufPackage = "users";

export interface Empty {
}

export interface Error {
  message: string;
}

export interface User {
  id: string;
  name: string;
  lastname: string;
  email: string;
}

export interface TokenResponse {
  token: string;
  error: Error | undefined;
}

export interface CreateUserRequest {
  name: string;
  lastname: string;
  email: string;
  password: string;
}

export interface LoginRequest {
  email: string;
  password: string;
}

export interface GetUserByEmailRequest {
  email: string;
}

export interface GetUserByEmailResponse {
  id: string;
  name: string;
  lastname: string;
  email: string;
}

export const USERS_PACKAGE_NAME = "users";

export interface UsersServiceClient {
  signUp(request: CreateUserRequest): Observable<GetUserByEmailResponse>;

  signIn(request: LoginRequest): Observable<GetUserByEmailResponse>;

  getUserByEmail(request: GetUserByEmailRequest): Observable<GetUserByEmailResponse>;
}

export interface UsersServiceController {
  signUp(
    request: CreateUserRequest,
  ): Promise<GetUserByEmailResponse> | Observable<GetUserByEmailResponse> | GetUserByEmailResponse;

  signIn(
    request: LoginRequest,
  ): Promise<GetUserByEmailResponse> | Observable<GetUserByEmailResponse> | GetUserByEmailResponse;

  getUserByEmail(
    request: GetUserByEmailRequest,
  ): Promise<GetUserByEmailResponse> | Observable<GetUserByEmailResponse> | GetUserByEmailResponse;
}

export function UsersServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["signUp", "signIn", "getUserByEmail"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("UsersService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("UsersService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const USERS_SERVICE_NAME = "UsersService";
