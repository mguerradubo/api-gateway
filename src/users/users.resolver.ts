import { Args, Mutation, Resolver, Query} from '@nestjs/graphql';
import {Inject, OnModuleInit} from '@nestjs/common';
import {ClientGrpc} from '@nestjs/microservices';
import { Observable, map } from 'rxjs';
import { UsersServiceClient, CreateUserRequest, GetUserByEmailRequest, GetUserByEmailResponse, TokenResponse, LoginRequest} from './users.pb';
import { AuthGuard } from './auth.guard';
import { UseGuards } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

@Resolver('Users')
export class UsersResolver implements OnModuleInit {
  private usersService: UsersServiceClient;
  constructor(@Inject('Users') private client: ClientGrpc) {}
  onModuleInit() {
    this.usersService = this.client.getService<UsersServiceClient>('UsersService');
  }

  createToken(email: string, name: string){
    return jwt.sign({ email, name}, 'secret',{expiresIn: '1h'});
  }

  @Mutation('signUp')
  async createUser(@Args('input') input: CreateUserRequest){
    let email: string;
    let name: string;

    this.usersService.signUp(input).subscribe(user => {
        email = user.email;
        name = user.name;
    });

    return this.createToken(email, name);
}

  @Mutation('login')
  async login(@Args('input') input: LoginRequest){
    const user = await this.usersService.signIn(input)

    if(user){
      let email: string;
      let name: string;
  
      this.usersService.signIn(input).subscribe(user => {
          email = user.email;
          name = user.name;
      });
  
      return this.createToken(email, name);
    }
  }

  @Query()
  @UseGuards(new AuthGuard())
  user(@Args('input') input: GetUserByEmailRequest){
    return this.usersService.getUserByEmail(input)
  }
}
