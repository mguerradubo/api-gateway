import { Module } from '@nestjs/common';
import { RecipesModule } from './recipes/recipes.module';
import { UsersModule } from './users/users.module';
import { CommentsModule } from './comments/comments.module';
import { CategoriesModule } from './categories/categories.module';
import { IngredientsModule } from './ingredients/ingredients.module';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';

@Module({
  imports: [
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      installSubscriptionHandlers: true,
      typePaths: ['./**/*.graphql'],
      context: ({ req, res }) => ({ req, res }),
    }),
    RecipesModule,
    UsersModule,
    CommentsModule,
    CategoriesModule,
    IngredientsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
