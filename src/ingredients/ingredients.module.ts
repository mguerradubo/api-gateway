import { Module } from '@nestjs/common';
import { IngredientsResolver } from './ingredients.resolver';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import * as dotenv from 'dotenv';
import { protobufPackage } from './ingredients.pb';

dotenv.config()
@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'Ingredients',
        transport: Transport.GRPC,
        options: {
          url: process.env.MS_RECIPE_URL,
          package: protobufPackage,
          protoPath: join('node_modules/protos/proto/ingredients.proto'),
        },
      }
    ])
  ],
  providers: [IngredientsResolver,],
})
export class IngredientsModule {}
