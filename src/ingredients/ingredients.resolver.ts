import { Inject, OnModuleInit } from '@nestjs/common';
import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { ClientGrpc } from '@nestjs/microservices';
import { Observable, map } from 'rxjs';
import { IngredientsServiceClient, IngredientResponse, Ingredient, FindRequest, InputLimit, AllIngredients} from './ingredients.pb';

@Resolver('Ingredients')
export class IngredientsResolver implements OnModuleInit{
  private ingredientsService: IngredientsServiceClient;
  constructor(@Inject('Ingredients') private client: ClientGrpc) {}
  onModuleInit() {
    this.ingredientsService = this.client.getService<IngredientsServiceClient>('IngredientsService');
  }

  @Mutation('loadIngredients')
  async loadIngredients(): Promise<Observable<IngredientResponse>> {
    return this.ingredientsService.loadIngredients({})
  }

  @Query('ingredients')
  async findAll(): Promise<Observable<AllIngredients>> {
    return this.ingredientsService.findAll({})
  }

  @Query('ingredient')
  async findOne(@Args('input') input: FindRequest): Promise<Observable<Ingredient>> {
    return this.ingredientsService.findOne(input)
  }

  @Query('random_ingredient')
  async findRandom(@Args('input') input: InputLimit): Promise<Observable<AllIngredients>> {
    return this.ingredientsService.findRandom(input)
  }
}
